class LoginPageLocators:
    mail_field_id = "email"
    password_field_id = "password"
    login_button_xpath = "//button[@class='Button_button__KXFVC undefined']"


class NavBarLocators:
    user_menu_xpath = "//a[@class='dropdown-toggle nav-link']"
    logout_option_xpath = "//a[@class='dropdown-item']"
