import time
import unittest

from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.chrome.options import Options
from webdriver_manager.chrome import ChromeDriverManager

from Pages.loginPage import LoginPage
from Pages.navegationBar import NavigationBar


class LoginTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.options = Options()
        cls.options.add_experimental_option("detach", True)
        cls.driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()),
                                      options=cls.options)

    def test_login_logout(self):
        driver = self.driver
        driver.get("http://localhost:3000/")
        login = LoginPage(driver)
        navbar = NavigationBar(driver)
        login.enter_email("sistema@mail.com")
        login.enter_password("123")
        login.click_login()
        navbar.expand_user_menu()
        navbar.click_logout()

    @classmethod
    def tearDownClass(cls):
        cls.driver.close()
        cls.driver.quit()
