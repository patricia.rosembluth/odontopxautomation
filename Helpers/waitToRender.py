from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait


class WaitToRender:
    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, timeout=3, poll_frequency=1, ignored_exceptions=[NoSuchElementException])

    def element_by_id(self, element_id):
        element = self.wait.until(
            expected_conditions.visibility_of_element_located((By.ID, element_id)))
        return element

    def element_by_xpath(self, element_xpath):
        element = self.wait.until(
            expected_conditions.visibility_of_element_located((By.XPATH, element_xpath)))
        return element
