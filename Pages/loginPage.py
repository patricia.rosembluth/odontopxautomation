from selenium.common import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

from Helpers.waitToRender import WaitToRender
from Locators.pageLocators import LoginPageLocators


class LoginPage:
    def __init__(self, driver):
        self.driver = driver

    def enter_email(self, email):
        email_field = WaitToRender(self.driver).element_by_id(LoginPageLocators.mail_field_id)
        email_field.clear()
        email_field.send_keys(email)

    def enter_password(self, password):
        password_field = WaitToRender(self.driver).element_by_id(LoginPageLocators.password_field_id)
        password_field.clear()
        password_field.send_keys(password)

    def click_login(self):
        login_button = WaitToRender(self.driver).element_by_xpath(LoginPageLocators.login_button_xpath)
        login_button.click()
