from selenium.webdriver.common.by import By

from Helpers.waitToRender import WaitToRender
from Locators.pageLocators import NavBarLocators


class NavigationBar:
    def __init__(self, driver):
        self.driver = driver

    def expand_user_menu(self):
        user_menu = WaitToRender(self.driver).element_by_xpath(NavBarLocators.user_menu_xpath)
        user_menu.click()

    def click_logout(self):
        logout_button = WaitToRender(self.driver).element_by_xpath(NavBarLocators.logout_option_xpath)
        logout_button.click()

